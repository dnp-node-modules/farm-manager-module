'use strict';

module.exports = FarmManager;

var bluebird = require('bluebird');
var requester = require('./requester');

function FarmManager(url) {
  if (!(this instanceof FarmManager))
    return new FarmManager(url);

  var req = requester(url);
  this.createSystem = function (key, opts) {
    var options = (opts || {});

    options.json = true;
    options.body = {key: key, dedicated: options.dedicated};

    return req('POST', '/farm-nodes/allocate-system', options);
  };

  this.destroySystem = function (nodeAppId, systemId) {
    return req('DELETE', '/farm-nodes/' + nodeAppId + '/systems/' + systemId);
  };

  this.isolate = function (nodeAppId, systemId) {
    return bluebird.reject(new Error('Not Implemented'));
  };

  this.moveOut = function (nodeAppId, systemId) {
    return bluebird.reject(new Error('Not Implemented'));
  };
}
