Farm Manager module for node.js
===============================

Access farm manager.

## API

### FarmManager(String url)

Initialize a FarmManager object.

### .createSystem(String key, [Object options])

Creates a new system into an existing farm node or in a new node.
Return a farm-node-system object.
`options.dedicated` indicates if the system should be created on a dedicated
node or not.

### .destroySystem(String nodeAppId, String systemId)

Deinit a system and mark it for later destruction.

### .isolate(String nodeAppId, String systemId)

Move all siblings systems to other nodes, isolating the given system.

### .moveOut(String nodeAppId, String systemId)

Move the given system out of it's current node.
