'use strict';

var url = require('url');
var merge = require('@dnp/utils').merge;
var request = require('request');
var inherits = require('util').inherits;
var bluebird = require('bluebird');

module.exports = FarmManagerRequester;

function FarmManagerRequester(managerUrl) {
  return function requester(method, path, options) {
    var uri = getURI(managerUrl, path);
    var opts = merge(options || {}, {
      url: uri,
      method: method
    });

    return new bluebird(function (resolve, reject) {
      request(opts, function () { /* request() requires a callback to parse response body */ })
      .on('error', reject)
      .once('complete', resolve);
    })
    .then(function (res) {
      if (res.statusCode >= 400 && res.statusCode < 500)
        throw new HTTPClientError(res.statusCode, uri.path, res.body);

      if (res.statusCode >= 500 && res.statusCode < 600)
        throw new HTTPServerError(res.statusCode, uri.path, res.body);

      return res.body;
    });
  };
}

inherits(HTTPClientError, Error);
function HTTPClientError(statusCode, url, body) {
  Error.call(this);
  Error.captureStackTrace(this, HTTPClientError);

  this.code = statusCode;
  this.url = url;
  this.response = body;
  this.message = 'Unexpected response code "' + this.code + '"; ' +
                 'response: ' + (this.response || '(no content)');
}

inherits(HTTPServerError, Error);
function HTTPServerError(statusCode, url, body) {
  Error.call(this);
  Error.captureStackTrace(this, HTTPServerError);

  this.code = statusCode;
  this.url = url;
  this.response = body;
  this.message = 'Unexpected response code "' + this.code + '"; ' +
                 'response: ' + (this.response || '(no content)');
}

function getURI(managerUrl, path) {
  var uri = url.parse(managerUrl);
  uri.pathname = path;
  uri.path = path;
  return uri;
}
